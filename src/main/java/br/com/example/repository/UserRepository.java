package br.com.example.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.example.entity.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

	public List<User> findByEnable(final boolean enable);

	public User findByLogin(final String login);

}
