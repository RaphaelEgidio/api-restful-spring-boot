package br.com.example.system;

import java.io.Serializable;
import java.util.Date;

import lombok.Data;

@Data
public class ErrorMessage implements Serializable {

	private static final long serialVersionUID = 898489315668009511L;

	private Date date;
	private String message;
	private int httpStatus;

	public ErrorMessage(Date date, String message, int httpStatys) {
		this.date = date;
		this.message = message;
		this.httpStatus = httpStatys;
	}

}
