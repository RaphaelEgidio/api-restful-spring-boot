package br.com.example.system;

import java.util.Date;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice(annotations = RestController.class)
public class RestResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

	@ExceptionHandler(GenericException.class)
	protected ResponseEntity<Object> handleException(RuntimeException runtimeException, WebRequest request) {
		return handleExceptionInternal(runtimeException,
				buildMessageError(runtimeException, HttpStatus.INTERNAL_SERVER_ERROR), new HttpHeaders(),
				HttpStatus.INTERNAL_SERVER_ERROR, request);
	}

	private ErrorMessage buildMessageError(RuntimeException runtimeException, HttpStatus httpStatus) {
		return new ErrorMessage(new Date(), runtimeException.getMessage(), httpStatus.value());
	}

}
