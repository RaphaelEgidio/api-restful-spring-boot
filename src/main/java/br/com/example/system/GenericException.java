package br.com.example.system;

public class GenericException extends Exception {

	private static final long serialVersionUID = -777796943829690587L;

	public GenericException() {
		super();
	}

	public GenericException(String message) {
		super(message);
	}

	public GenericException(String message, Throwable cause) {
		super(message, cause);
	}

	public GenericException(Throwable cause) {
		super(cause);
	}

}