package br.com.example.api;

import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8_VALUE;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.example.entity.User;
import br.com.example.service.UserService;
import br.com.example.system.GenericException;

@RestController
@RequestMapping("/user")
public class UserController implements BasicCrud<User> {

	@Autowired
	private UserService userService;

	@RequestMapping(value = "/save", method = POST, consumes = APPLICATION_JSON_UTF8_VALUE)
	public User save(@RequestBody User entity) throws GenericException {
		try {
			return userService.save(entity);
		} catch (Exception e) {
			e.printStackTrace();
			throw new GenericException("Não foi possivel salvar usuário!");
		}
	}

	@RequestMapping(value = "/findOne/{id}", method = GET, produces = APPLICATION_JSON_UTF8_VALUE)
	public User findOne(@PathVariable("id") Long id) throws GenericException {
		try {
			return userService.findOne(id);
		} catch (Exception e) {
			e.printStackTrace();
			throw new GenericException("Não foi possivel carregar usuário!");
		}
	}

	@RequestMapping(value = "/findAll/{enable}", method = GET, produces = APPLICATION_JSON_UTF8_VALUE)
	public List<User> findAll(@PathVariable("enable") Boolean enable) throws GenericException {
		try {
			return userService.findAll(enable);
		} catch (Exception e) {
			e.printStackTrace();
			throw new GenericException("Não foi possivel buscar os usuários!");
		}
	}

	@RequestMapping(value = "/delete/{id}", method = GET, produces = APPLICATION_JSON_UTF8_VALUE)
	public void delete(@PathVariable("id") Long id) throws GenericException {
		try {
			userService.disableOrEnable(id);
		} catch (Exception e) {
			e.printStackTrace();
			throw new GenericException("Não foi possivel desativar usuário!");
		}
	}

}
