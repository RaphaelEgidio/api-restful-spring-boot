package br.com.example.api;

import java.util.List;

import br.com.example.system.GenericException;

public interface BasicCrud<T> {

	public T save(T entity) throws GenericException;

	public T findOne(Long id) throws GenericException;

	public List<T> findAll(Boolean enable) throws GenericException;

	public void delete(Long id) throws GenericException;

}
